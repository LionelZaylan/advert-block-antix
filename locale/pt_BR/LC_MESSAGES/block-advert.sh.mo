��          T      �       �   ,   �      �             y   3  �  �  �  Q  A     *   M     x  *   �  �   �  ?  g                                        Choose your preferred ad blocking service(s) Loading  adlist from $domain No item selected Restoring original /etc/hosts. Success - your settings have been changed.\n\nYour hosts file has been updated.\nRestart your browser to see the changes. The <b>$title</b> tool adds stuff to your /etc/hosts file, so \nthat many advertising servers and websites will not be able to connect \nto this PC.\n\nYou can choose one service or combine multiple services for more advert protection.\nBlocking ad servers protects your privacy, saves you bandwidth, greatly \nimproves web-browsing speed and makes the internet much less annoying in general.\n\nDo you want to proceed? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-11-28 16:46+0200
Last-Translator: marcelo cripe <marcelocripe@gmail.com>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/anticapitalista/antix-development/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 Escolha os seus serviço(s) de bloqueio de anúncios preferido(s) Carregando a lista de anúncios de $domain Nenhum item selecionado Restaurando o arquivo original /etc/hosts. As suas configurações foram alteradas com sucesso. \n\nO seu arquivo hospedeiro (hosts) foi atualizado. \n Reinicie o seu navegador de internet para ver as alterações. A ferramenta <b>$title</b> adiciona informações ao seu \narquivo /etc/hosts, para que muitos servidores de publicidade e sítios/\nsites da internet (websites) não consigam se conectar ao seu \ncomputador.\n\nVocê pode escolher um serviço ou combinar vários serviços para obter \nmais proteção contra anúncios publicitários.\nBloquear servidores que enviam anúncios, protege a sua privacidade,\n  economiza largura de banda de internet, melhora substancialmente a \nvelocidade de navegação e torna a Internet muito menos incômoda. \n\nVocê pretende continuar? 