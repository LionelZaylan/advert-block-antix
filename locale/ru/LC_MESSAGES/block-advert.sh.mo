��          T      �       �   ,   �      �             y   3  �  �  ?  Q  }   �  7         G  5   h  �   �  �  k                                        Choose your preferred ad blocking service(s) Loading  adlist from $domain No item selected Restoring original /etc/hosts. Success - your settings have been changed.\n\nYour hosts file has been updated.\nRestart your browser to see the changes. The <b>$title</b> tool adds stuff to your /etc/hosts file, so \nthat many advertising servers and websites will not be able to connect \nto this PC.\n\nYou can choose one service or combine multiple services for more advert protection.\nBlocking ad servers protects your privacy, saves you bandwidth, greatly \nimproves web-browsing speed and makes the internet much less annoying in general.\n\nDo you want to proceed? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-11 18:46+0200
Last-Translator: Вячеслав Волошин <vol_vel@mail.ru>
Language-Team: Russian (http://www.transifex.com/anticapitalista/antix-development/language/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 2.2.1
 Выберите сервис(ы)  для блокировки рекламы, которые Вы предпочитаете Загрузка списка рекламы с $domain Не выбран элемент Восстановить исходный /etc/hosts. Успешно - Ваши настройки были изменены.\n\nВаш файл hosts обновлен.\nПерезапустите браузер, чтобы увидеть изменения. <b>$title</b> инструмент добавляет некоторые вещи в файл /etc/hosts, благодаря чему \nмногие рекламные серверы и веб-сайты не смогут подключиться \nк этому компьютеру.\n\nВы можете выбрать один сервис или объединить несколько для получения дополнительной защиты от рекламы.\nБлокирование рекламных серверов защищает Вашу конфиденциальность, позволяет сэкономить полосу пропускания, значительно \nулучшает скорость открытия страниц и делает интернет менее раздражающим в целом.\n\nВы хотите продолжить? 