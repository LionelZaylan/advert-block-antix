��          T      �       �   ,   �      �             y   3  �  �  �  Q  2     '   7     _  )   {  �   �  �  5                                        Choose your preferred ad blocking service(s) Loading  adlist from $domain No item selected Restoring original /etc/hosts. Success - your settings have been changed.\n\nYour hosts file has been updated.\nRestart your browser to see the changes. The <b>$title</b> tool adds stuff to your /etc/hosts file, so \nthat many advertising servers and websites will not be able to connect \nto this PC.\n\nYou can choose one service or combine multiple services for more advert protection.\nBlocking ad servers protects your privacy, saves you bandwidth, greatly \nimproves web-browsing speed and makes the internet much less annoying in general.\n\nDo you want to proceed? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-11 18:43+0200
Last-Translator: Pierluigi Mario <pierluigimariomail@gmail.com>
Language-Team: Italian (http://www.transifex.com/anticapitalista/antix-development/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.1
 Scegli il tuo servizio(i) preferito di ad blocking Caricamento ad list da $domain in corso Nessun elemento selezionato Ripristino del file originale /etc/hosts. Successo - le tue impostazioni sono state cambiate.\n\nIl tuo hosts file è stato aggiornato.\nRiavvia il tuo browser per vedere i cambiamenti. Lo strumento <b>$title</b> aggiunge voci al tuo file /etc/hosts, così \nche tanti servers pubblicitari e siti web non saranno in grado di connettersi  \na questo PC.\n\nTu puoi scegliere un servizio o combinare più servizi per una maggiore protezione dalla pubblicità.\nIl blocco ai servers protegge la tua privacy, consente di risparmiare larghezza di banda, \nincrementa notevolmente la velocità di presentazione delle pagine web e rende Internet molto meno fastidioso in generale.\n\nVuoi procedere? 