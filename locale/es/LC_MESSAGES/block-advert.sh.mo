��          T      �       �   ,   �      �             y   3  �  �  �  Q  6     )   8  #   b     �  �   �  �  /                                        Choose your preferred ad blocking service(s) Loading  adlist from $domain No item selected Restoring original /etc/hosts. Success - your settings have been changed.\n\nYour hosts file has been updated.\nRestart your browser to see the changes. The <b>$title</b> tool adds stuff to your /etc/hosts file, so \nthat many advertising servers and websites will not be able to connect \nto this PC.\n\nYou can choose one service or combine multiple services for more advert protection.\nBlocking ad servers protects your privacy, saves you bandwidth, greatly \nimproves web-browsing speed and makes the internet much less annoying in general.\n\nDo you want to proceed? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-11 18:38+0200
Last-Translator: German Lancheros <glancheros2015@gmail.com>
Language-Team: Spanish (http://www.transifex.com/anticapitalista/antix-development/language/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.1
 Escoja su servicio(s) preferido para bloquear anuncios Cargando la lista de anuncios del $domain No se ha seleccionado ningún ítem Restaurar /etc/hosts original. Éxito -- su configuración se ha cambiado.\n\nSu archivo de hosts se ha actualizado.\nReinicie su navegador para visualizar los cambios. La herramienta <b>$title</b>, agrega elementos al archivo /etc/hosts, para \nque muchos servidores y páginas web de publicidad no puedan conectar \na esta PC. \n\nPuede escoger un servicio o combinar múltiples servicios para mayor protección. \nBloqueando servidores de publicidad protege su privacidad, ahorra ancho de banda y hace que Internet sea mucho menos molesto en general.\n\n ¿Deseas proceder? 