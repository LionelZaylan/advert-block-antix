��          T      �       �   ,   �      �             y   3  �  �  �  Q  3   �      .     O  !   c  �   �  �                                          Choose your preferred ad blocking service(s) Loading  adlist from $domain No item selected Restoring original /etc/hosts. Success - your settings have been changed.\n\nYour hosts file has been updated.\nRestart your browser to see the changes. The <b>$title</b> tool adds stuff to your /etc/hosts file, so \nthat many advertising servers and websites will not be able to connect \nto this PC.\n\nYou can choose one service or combine multiple services for more advert protection.\nBlocking ad servers protects your privacy, saves you bandwidth, greatly \nimproves web-browsing speed and makes the internet much less annoying in general.\n\nDo you want to proceed? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-11 18:48+0200
Last-Translator: Henry Oquist <henryoquist@comhem.se>
Language-Team: Swedish (http://www.transifex.com/anticapitalista/antix-development/language/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.1
 Välj den annonsblockerings-service(r) du föredrar Laddar annonslista från $domain Inget objekt utvalt Återställa original /etc/hosts. Framgång - dina inställningar har blivit ändrade.\n\nDin hosts fil har blivit uppdaterad.\nStarta om din webläsare för att se ändringarna. <b>$title</b> verktyget lägger till saker till din /etc/hosts fil, \nså många annonsservrar och websidor inte kommer att kunna ansluta \ntill denna PC.\n\nDu can välja en service eller kombinera flera servicer för mer skydd.\nBlockera annonsservrar skyddar ditt privatliv, spar bandvidd, \nförbättrar webläsarens hastighet och gör internet allmänt sett mindre irriterande.\n\nVill du fortsätta? 