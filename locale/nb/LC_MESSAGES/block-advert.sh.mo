��          T      �       �   ,   �      �             y   3  �  �  �  Q  3   	     =     ]  "   p  �   �  �                                          Choose your preferred ad blocking service(s) Loading  adlist from $domain No item selected Restoring original /etc/hosts. Success - your settings have been changed.\n\nYour hosts file has been updated.\nRestart your browser to see the changes. The <b>$title</b> tool adds stuff to your /etc/hosts file, so \nthat many advertising servers and websites will not be able to connect \nto this PC.\n\nYou can choose one service or combine multiple services for more advert protection.\nBlocking ad servers protects your privacy, saves you bandwidth, greatly \nimproves web-browsing speed and makes the internet much less annoying in general.\n\nDo you want to proceed? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-06-16 15:16+0300
Last-Translator: Kjell Cato Heskjestad <cato@heskjestad.xyz>
Language-Team: Norwegian Bokmål (http://www.transifex.com/anticapitalista/antix-development/language/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Velg en eller flere tjenester for reklameblokkering Laster reklameliste fra $domain Ingenting er valgt Gjenoppretter original /etc/hosts. Fullført – innstillingene er endret.\n\nHosts-filen er oppdatert.\nStart nettleseren din på nytt for å ta i bruk endringene. Verktøyet <b>$title</b> legger til innhold i filen /etc/hosts\nslik at mange reklametjenere og -nettsteder ikke kan koble seg\ntil denne PC-en. Du kan velge én tjeneste eller kombinere flere\n for utvidet reklamebeskyttelse. Dette beskytter personvernet ditt\n, begrenser bruken av båndbredde, øker nettlesingshastigheten\nog gjør Internett mindre irriterende.\n\nVil du fortsette? 