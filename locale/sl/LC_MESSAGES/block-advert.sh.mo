��          T      �       �   ,   �      �             y   3  �  �  �  Q  :   1     l     �     �  �   �  �  I                                        Choose your preferred ad blocking service(s) Loading  adlist from $domain No item selected Restoring original /etc/hosts. Success - your settings have been changed.\n\nYour hosts file has been updated.\nRestart your browser to see the changes. The <b>$title</b> tool adds stuff to your /etc/hosts file, so \nthat many advertising servers and websites will not be able to connect \nto this PC.\n\nYou can choose one service or combine multiple services for more advert protection.\nBlocking ad servers protects your privacy, saves you bandwidth, greatly \nimproves web-browsing speed and makes the internet much less annoying in general.\n\nDo you want to proceed? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-11 18:47+0200
Last-Translator: Arnold Marko <arnold.marko@gmail.com>
Language-Team: Slovenian (http://www.transifex.com/anticapitalista/antix-development/language/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Generator: Poedit 2.2.1
 Izberite želene storitve (storitev) za blokiranja oglasov Nalaganje adlist iz $domain Nič ni izbrano Obnova originalnega /etc/hosts. Sprememba nastavitev je uspela.

Vaše gostiteljska datoteka je bila posodobljena.
Ponovno zaženite spletni brskalnik, da bodo spremembe vidne. Orodje <b>$title</b> doda stvari v vašo /etc/hosts datoteko,
tako da se mnogo oglaševalskih strežnikov in spletnih strani ne bo moglo
povezati s gtem računalnikom.

Lahko izberete eno storitev ali kombinirate več storitev za več zaščite pred oglaševanjem.
Blokiranje oglaševalskih strežnikov sčiti vašo zasebnost, manjša pretok podatkov,
zelo poveča hitrost brskanja in naredi splet veliko manj zoprn.

Ali želite nadaljevati? 